IPV6 RIPNG
WRITTEN BY RENE MOLENAAR ON 31 AUGUST 2011. POSTED IN IPV6
SCENARIO:
After waking up from cryosleep the year appears to be 2020 and there is not a single IPv4 address left on the planet. The last thing you remember are the stories about the end of IPv4 and the migration plans for IPv6...now it seems this is all reality! It's up to you to configure RIPNG and make the network operational...resistance is futile ;)

GOAL:
You are not allowed to use IPv4 addresses.
You are not allowed to use any IPv6 global unicast addresses except the ones on the loopback interfaces.
Use the MAC-address for the last 64 bits of the IPv6 addresses required for the FastEthernet links.
Configure RIPNG process "GNS3VAULT" and achieve full connectivity between the networks on the loopback interfaces.
IOS:
c3640-jk9s-mz.124-16.bin
