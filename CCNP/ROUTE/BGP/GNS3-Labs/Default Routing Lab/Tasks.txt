Learning Objective:
In this lab, you will configure BGP to exchange routing information with two Internet Service Providers (ISPs).

Scenario:
The International Travel Agency relies extensively on the Internet for sales. The
company has contracted with two ISPs for Internet connectivity with fault
tolerance. You need to configure BGP, which runs between the San Jose
boundary router and the two ISP routers.


Step 1: Assign IP Addresses
Name the routers per the diagram. Disable domain-lookup.
Enable synchronous logging and set exec-timeout to 0 on the console 0 line.
Configure the network according to the diagram, but do not configure a routing
protocol. Configure a loopback interface with an IP address for each ISP route
shown in the diagram. These loopbacks simulate real networks that can be
reached through the ISP. Configure two loopback interfaces with the IP
addresses for the San Jose router. These loopbacks simulate the connections
between the core routers.

Use ping to test the connectivity between the directly connected routers.
Note that router ISP1 cannot reach router ISP2.

Step 2: Configure BGP on the ISPs
Configure the ISP routers for BGP, and the International Travel Agency�s boundary router,
San Jose. Advertise the LOOPBACK interfaces on each router into BGP.

Step 3: Configure SanJose BGP
Configure the San Jose router to run BGP with both providers

To verify the configuration, check the routing table for SanJose with the 'show ip route' command

Verify that
SanJose has connectivity to these networks by pinging each loopback address
from its console. If these pings are not successful, troubleshoot.

Step 4: Verify BGP on the SanJose Router
To verify the operation of SanJose, issue the 'show ip bgp' command

What is the local router ID?
Which table version is displayed?

On the ISP1 router, issue the shutdown command on Loopback 0. Then on
SanJose, issue the 'show ip bgp' command again.

Which table version is displayed?

Bring ISP1 router Loopback0 back. On SanJose, issue the 'show ip bgp neighbors' command. 

Based on the output of this command, what is the BGP state between this router and ISP2?

How long has this connection been up?

Step 5: Filter Routes
Check ISP2�s routing table using the 'show ip route' command. ISP2 should have a route that belongs to ISP1, network 201.0.1.0/24.
If SanJose advertises a route belonging to ISP1, ISP2 installs that route in its table. 
ISP2 might then attempt to route transit traffic through the International Travel Agency.
Configure the SanJose router so that it advertises only International Travel Agency networks 192.168.0.0 and 192.168.1.0 to both providers.

Then apply this access list as a route filter using the distribute-list keyword with the BGP neighbor statement

After you configured the route filter, check the routing table for ISP2 again. The route to 201.0.1.0, ISP1, should still be in the table.
Return to SanJose and issue the 'clear ip bgp * out' command.
Then recheck the ISP2 routing table.
The route to ISP1 should no longer be in the routing table. The route to ISP2, network 203.0.0.0/24, should not be in the routing table for ISP1.

Step 6: Configure the Primary and Backup Default Routes Using Floating Static Routes 
Now that bidirectional communication has been established with each ISP via BGP, it is time to configure the primary and backup default routes.
This can be done with floating static routes or BGP. 

Configure the floating static route method

Now verify that a default route is defined using the 'show ip route' command

Test this default route by creating an unadvertised loopback on the router for ISP1. Use 210.210.210.1/24 as the ip for the loopback

Issue the 'show ip route' command to insure that the newly added
210.210.210.0 /24 network does not appear in the routing table

Ping the 210.210.210.1 loopback interface originating from the 192.168.1.1 SanJose interface

Step 7: Configure Primary and Backup Routes Using Static Routes
Another method for configuring primary and backup routes is to use the default-network command instead of a 0.0.0.0/0 route.

Remove the floating static routes configured in Step 6

Now advertise The network that was added in the last step, 210.210.210.0/24 on the ISP1 router.

Configure the SanJose router a default-network statement to reestablish a Gateway of Last Resort.
Make sure that the classful network 210.210.210.0 /24 appears in the routing table and is followed with the '*-candidate default'

Wait a few moments, and then re-examine the routing table on SanJose


This establishes ISP1 as the only default route. This route can be manipulated with policy routing.
Correct this by adding a backup route to the 172.16.0.2 (ISP2).


Notice that the routing table includes two candidate default routes (*), only one of which is used because of different administrative distances.




