#!/bin/bash
echo "Beginning IPv4 TFTP Transfer"
atftp --verbose --option "blksize 8192" --get -r vios-adventerprisek9-m 109.65.200.241 69
echo "Cisco routers don't support IPv6 TFTP Server"
# atftp --verbose --option "blksize 8192" --get -r vios-adventerprisek9-m 2002::109:65:200:241 69
